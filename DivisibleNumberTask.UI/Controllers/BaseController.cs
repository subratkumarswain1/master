﻿using System;
using System.Diagnostics;
using System.Web.Mvc;

namespace DivisibleNumberTask.UI.Controllers
{
    public abstract class BaseController<TController> : Controller
    {
        protected string ControllerName => this.ControllerContext.RouteData.Values["controller"].ToString();
        protected string ActionName => this.ControllerContext.RouteData.Values["action"].ToString();
        protected void Try(Action action)
        {            
            try
            {
                action();
            }
            catch(Exception exception)
            {
                //We can log any error from here. For the time I am logging into Debug console
                Debug.WriteLine(exception);
                Debug.WriteLine(ControllerName);
                Debug.WriteLine(ActionName);
            }
        }
    }
}