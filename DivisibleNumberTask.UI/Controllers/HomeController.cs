﻿using System.Collections.Generic;
using System.Web.Mvc;
using DivisibleNumber.Business;
using DivisibleNumber.Business.Interface;
using DivisibleNumber.ViewModel;
using System.Linq;

namespace DivisibleNumberTask.UI.Controllers
{
    public class HomeController : BaseController<HomeController>
    {
        private IDivisibleBusiness _iDivisibleBusiness = new DivisibleBusiness();

        public ActionResult Index(DivisibleViewModel vm)
        {
            var result = new DivisibleViewModel();
            Try(() =>
            {
                var divisibleNumber = new Dictionary<int, string> { { 3, string.Empty }, { 5, string.Empty }, { 15, string.Empty } };
                result.NumberLists = _iDivisibleBusiness.GetFizzBuzzCombinationValues(divisibleNumber, _iDivisibleBusiness.ConvertToSequenceNumer(vm.InputNumber));
                result.InfoMessage = result.NumberLists.Count > 0 ? "Processed successfully." : "Invalid Input, Please input only 1-1000 number.";
            });
            return View(result);
        }
    }
}