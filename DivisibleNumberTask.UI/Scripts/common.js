﻿function removeSpaceAndSpecialCharOnKeyup(controlId) {
    $("#" + controlId).keyup(function () {
        $(this).val($(this).val().replace(/[_\W]+/g, ""));
    });
}
function isValidNumber(id) {
    $("#" + id).keyup(function () {
        if (/\D/g.test(this.value)) {
            this.value = this.value.replace(/\D/g, "");
        }
    });
}