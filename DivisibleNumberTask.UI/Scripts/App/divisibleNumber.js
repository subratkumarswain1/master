﻿$(function () {
    isValidNumber("inputNumber");
    removeSpaceAndSpecialCharOnKeyup("inputNumber");
})
function validate() {
    var inputValue = $("#inputNumber").val();
    if (inputValue != '' || inputValue != null) {
        if (parseInt(inputValue) >= 1 && parseInt(inputValue) <= 1000) {
            return true;
        } else {
            alert('Number must be between 1-1000');
            return false;
        }
    } else {
        alert('Number field shoud not be empty. Please enter a number between 1-1000')
        return false;
    }
}