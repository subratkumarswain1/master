﻿namespace DivisibleNumber.Constants
{
    public class AppConstants
    {
        public const string FIZZ = "fizz", BUZZ = "buzz", FIZZ_BUZZ = "Fizz buzz";
    }
}