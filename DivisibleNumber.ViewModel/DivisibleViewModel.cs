﻿using System.Collections.Generic;

namespace DivisibleNumber.ViewModel
{
    public class DivisibleViewModel : BaseViewModel
    {
        public int InputNumber { get; set; }
        public List<NumberList> NumberLists { get; set; } = new List<NumberList>();
    }
}