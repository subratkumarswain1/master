﻿namespace DivisibleNumber.ViewModel
{
   public class NumberList:BaseViewModel
    {
        public int Number { get; set; }
        public string Value { get; set; }
    }
}
