﻿
namespace DivisibleNumber.ViewModel
{
    public abstract class BaseViewModel : IViewModel
    {
        public string ErrorMessage { get; set; }
        public string WarningMessage { get; set; }
        public string InfoMessage { get; set; }
    }
}
