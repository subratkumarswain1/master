﻿
namespace DivisibleNumber.ViewModel
{
    interface IViewModel
    {
        string ErrorMessage { get; set; }
        string WarningMessage { get; set; }
        string InfoMessage { get; set; }
    }
}
