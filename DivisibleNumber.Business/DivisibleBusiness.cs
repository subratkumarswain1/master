﻿using DivisibleNumber.Business.Interface;
using DivisibleNumber.Constants;
using DivisibleNumber.ViewModel;
using System.Collections.Generic;

namespace DivisibleNumber.Business
{
    public class DivisibleBusiness : BaseBusiness, IDivisibleBusiness
    {
        public List<int> ConvertToSequenceNumer(int inputNumber)
        {
            var result = new List<int>();
            if (inputNumber > 0 && inputNumber <= 1000)
            {
                for (int i = 1; i <= inputNumber; i++)
                {
                    result.Add(i);
                }
                return result;
            }
            return result;
        }

        public List<NumberList> GetFizzBuzzCombinationValues(Dictionary<int, string> divisibleNumbers, List<int> inputValues = null, int number = 0)
        {
            var result = new List<NumberList>();
            if (number > 0) inputValues = ConvertToSequenceNumer(number);
            if (inputValues.Count > 0)
            {
                foreach (var inputValue in inputValues)
                {
                    var numberList = new NumberList();
                    foreach (var divisibleNumber in divisibleNumbers)
                    {
                        if (inputValue % divisibleNumber.Key == 0)
                        {
                            numberList.Number = inputValue;
                            numberList.Value = string.IsNullOrEmpty(divisibleNumber.Value) ? GetFizzBuzzValue(divisibleNumber.Key) : divisibleNumber.Value;
                        }
                        else numberList.Number = inputValue;
                    }
                    result.Add(numberList);
                }
            }
            return result;
        }

        public string GetFizzBuzzValue(int divisibleNumber)
        {
            if (divisibleNumber == 3)
                return AppConstants.BUZZ;
            else if (divisibleNumber == 5)
                return AppConstants.FIZZ;
            else if (divisibleNumber == 15)
                return AppConstants.FIZZ_BUZZ;
            else
                return string.Empty;
        }
    }
}