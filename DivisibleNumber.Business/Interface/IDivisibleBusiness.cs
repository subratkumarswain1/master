﻿using DivisibleNumber.ViewModel;
using System.Collections.Generic;

namespace DivisibleNumber.Business.Interface
{
    public interface IDivisibleBusiness
    {
         List<int> ConvertToSequenceNumer(int inputNumber);
         List<NumberList> GetFizzBuzzCombinationValues(Dictionary<int,string> divisibleNumbers, List<int> inputValues=null, int number=0);
        string GetFizzBuzzValue(int divisibleNumber);
    }
}
